package com.android.use.view;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ScaleDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.example.progressbutton.R;

public class ProgressButton extends RelativeLayout {
	private static final String TAG = "ProgressButton";

	private final int DEFAULT_BUTTON_TEXT = R.string.app_name;
	private final int DEFAULT_BUTTON_BG = R.drawable.pb_button_selector;
	private final int DEFAULT_PROGRESS_BG = R.drawable.pb_progressing_bg;
	private final int DEFAULT_WAITING_BG = R.color.percent_2_transpent;
	private final int DEFAULT_PROGRESS_DRAWABLE = R.drawable.pb_progress_horizontal;

	private OnProgressButtonListener mListener;
	private ProgressBar mProgressBar;
	private ProgressBar mWatingProgress;
	private Button mButton;

	public interface OnProgressButtonListener {
		void onProgressButtonClick();
	}

	public ProgressButton(Context context) {
		this(context, null);
	}

	public ProgressButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView();
	}

	private void initView() {
		setGravity(Gravity.CENTER);
		addView(makeProgressBar());
		addView(makeButton());
		addView(makeWaitingProgress());
	}

	private View makeButton() {
		mButton = new Button(this.getContext());
		mButton.setText(DEFAULT_BUTTON_TEXT);
		LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		mButton.setLayoutParams(lp);
		mButton.setBackgroundResource(DEFAULT_BUTTON_BG);
		mButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mListener != null) {
					mListener.onProgressButtonClick();
				}
			}
		});

		return mButton;
	}

	private View makeProgressBar() {
		mProgressBar = new ProgressBar(this.getContext(), null,
				android.R.style.Widget_ProgressBar_Horizontal);
		mProgressBar.setProgressDrawable(getResources().getDrawable(
				DEFAULT_PROGRESS_DRAWABLE));
		mProgressBar.setBackgroundResource(DEFAULT_PROGRESS_BG);
		LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		mProgressBar.setLayoutParams(lp);
		mProgressBar.setMax(100);
		mProgressBar.setProgress(40);
		return mProgressBar;
	}

	private View makeWaitingProgress() {
		mWatingProgress = new ProgressBar(this.getContext());
		mWatingProgress.setIndeterminateDrawable(getResources().getDrawable(
				R.drawable.pb_waiting_progress));
		mWatingProgress.setBackgroundResource(DEFAULT_WAITING_BG);
		LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		mWatingProgress.setLayoutParams(lp);
	    mWatingProgress.setVisibility(View.GONE);
		return mWatingProgress;
	}

	// http://m.oschina.net/blog/153569gr
	// http://www.cnblogs.com/xirihanlin/archive/2010/06/14/1758145.html
	public void setProgressColor(int color) {
		ScaleDrawable sd = (ScaleDrawable) ((LayerDrawable) mProgressBar
				.getProgressDrawable()).findDrawableByLayerId(R.id.progress);
		GradientDrawable gd = (GradientDrawable) sd.getDrawable();
		gd.setColor(color);
		// mProgressBar.setProgressDrawable(gd);
		// mProgressBar.setProgressDrawable(getResources().getDrawable(
		// R.drawable.pb_progress_horizontal2));
	}

	public void setOnProgressButtonListener(OnProgressButtonListener listener) {
		mListener = listener;
	}

	public void setProgressText(int textId) {
		if (textId <= 0) {
			throw new IllegalArgumentException("text id must > 0");
		}
		mButton.setText(textId);
	}

	public void setProgressText(String text) {
		if (TextUtils.isEmpty(text)) {
			throw new IllegalArgumentException("can not be empty");
		}
		mButton.setText(text);
	}

	public void setProgress(int progress) {
		mProgressBar.setProgress(progress);
		mButton.setText(String.valueOf(progress));
	}
	
	public void showWaitingProgress(boolean isShow) {
		if(isShow){
			mWatingProgress.setVisibility(View.VISIBLE);	
			mButton.setClickable(false);
		}else{
			mWatingProgress.setVisibility(View.GONE);
			mButton.setClickable(true);
		}		
	}

	public int getProgress() {
		return mProgressBar.getProgress();
	}

}
