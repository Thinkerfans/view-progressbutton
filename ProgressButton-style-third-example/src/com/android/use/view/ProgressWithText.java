package com.android.use.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ProgressBar;

public class ProgressWithText extends ProgressBar {

	private String mProgressText;
	private boolean mIsShowText = true;
	private Paint mPaint;

	public ProgressWithText(Context context) {
		super(context);
		initPaint();
	}

	public ProgressWithText(Context context, AttributeSet attrs) {
		super(context, attrs);
		initPaint();
	}

	private void initPaint() {
		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setColor(Color.RED);

	}

	public ProgressWithText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initPaint();
	}

	@Override
	public synchronized void setProgress(int progress) {
		setText(progress);
		super.setProgress(progress);

	}

	@SuppressLint("DrawAllocation")
	@Override
	protected synchronized void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(mIsShowText){
			Rect rect = new Rect();
			this.mPaint.getTextBounds(this.mProgressText, 0,
					this.mProgressText.length(), rect);
			int textCenterX = getWidth() / 2 - rect.centerX();
			int textCenterY = getHeight() / 2 - rect.centerY();
			canvas.drawText(mProgressText, textCenterX, textCenterY, mPaint);
		}

	}	
	// 设置文字内容
	private void setText(int progress) {
		int i = (int) ((progress * 1.0f / this.getMax()) * 100);
		this.mProgressText = String.valueOf(i) + " % ";
	}
	
	// 设置文字颜色
	public void setProgressTextColor(int color) {
		mPaint.setColor(color);
	}
	
	// 设置文字大小
	public void setProgressTextSize(float size) {
		mPaint.setTextSize(size);		
	}
	// 设置是否显示文字进度
	public void showProgressText(boolean isShow) {
		mIsShowText =isShow;		
	}
}
