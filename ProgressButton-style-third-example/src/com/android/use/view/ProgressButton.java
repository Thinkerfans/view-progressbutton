package com.android.use.view;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ScaleDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.progressbutton.R;

public class ProgressButton extends RelativeLayout {
	private static final String TAG = "ProgressButton";

	private final int DEFAULT_BUTTON_TEXT = R.string.app_name;
	private final int DEFAULT_BUTTON_BG = R.drawable.pb_button_selector;
	private final int DEFAULT_PROGRESS_BG = R.drawable.pb_progressing_bg;
	private final int DEFAULT_PROGRESS_DRAWABLE = R.drawable.pb_progress_horizontal;

	private OnProgressButtonListener mListener;
	private ProgressWithText mProgressBar;
	private TextView mTextView;

	public interface OnProgressButtonListener {
		void onProgressButtonClick();
	}

	public ProgressButton(Context context) {
		this(context, null);
	}

	public ProgressButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView();
	}

	private void initView() {
		setGravity(Gravity.CENTER);
		addView(makeProgressBar());
		addView(makeButton());

	}

	private View makeButton() {
		mTextView = new TextView(this.getContext());
		mTextView.setGravity(Gravity.CENTER);
		mTextView.setText(DEFAULT_BUTTON_TEXT);
		LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		mTextView.setLayoutParams(lp);
		mTextView.setBackgroundResource(DEFAULT_BUTTON_BG);
		mTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mListener != null) {
					mListener.onProgressButtonClick();
				}
			}
		});

		return mTextView;
	}

	private View makeProgressBar() {
		mProgressBar = new ProgressWithText(this.getContext(), null,
				android.R.style.Widget_ProgressBar_Horizontal);
		mProgressBar.setProgressDrawable(getResources().getDrawable(
				DEFAULT_PROGRESS_DRAWABLE));
		mProgressBar.setBackgroundResource(DEFAULT_PROGRESS_BG);
		LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		mProgressBar.setLayoutParams(lp);
		mProgressBar.setMax(100);
		mProgressBar.setProgress(40);
		return mProgressBar;
	}


	// http://m.oschina.net/blog/153569gr
	// http://www.cnblogs.com/xirihanlin/archive/2010/06/14/1758145.html
	public void setProgressColor(int color) {
		ScaleDrawable sd = (ScaleDrawable) ((LayerDrawable) mProgressBar
				.getProgressDrawable()).findDrawableByLayerId(R.id.progress);
		GradientDrawable gd = (GradientDrawable) sd.getDrawable();
		gd.setColor(color);
	}

	public void setOnProgressButtonListener(OnProgressButtonListener listener) {
		mListener = listener;
	}

	public void setProgressText(int textId) {
		if (textId <= 0) {
			throw new IllegalArgumentException("text id must > 0");
		}
		mTextView.setText(textId);
	}

	public void setProgressText(String text) {
		if (TextUtils.isEmpty(text)) {
			throw new IllegalArgumentException("can not be empty");
		}
		mTextView.setText(text);
	}

	public void setProgress(int progress) {
		mProgressBar.setProgress(progress);
	}
	
	public void setToWaitingProgress(boolean isShow) {
		if(isShow){
			mProgressBar.setProgressDrawable(getResources().getDrawable(
					R.drawable.pb_waiting_progress));	
			mTextView.setVisibility(View.GONE);
		}else{
			mProgressBar.setProgressDrawable(getResources().getDrawable(
					DEFAULT_PROGRESS_DRAWABLE));	
			mTextView.setVisibility(View.VISIBLE);
		}		
	}

}
