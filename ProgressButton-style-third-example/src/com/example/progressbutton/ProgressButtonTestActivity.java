package com.example.progressbutton;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.android.use.view.ProgressButton;
import com.android.use.view.ProgressButton.OnProgressButtonListener;

public class ProgressButtonTestActivity extends Activity implements
		OnProgressButtonListener {

	ProgressButton mPButton;
	boolean isDowning = false;
	int progress = 45;

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);

			if (msg.what == 2) {
				mPButton.setProgressText("暂停");
				mPButton.setToWaitingProgress(false);
			} else {

				if (progress != 100) {
					if (progress == 50) {
						mPButton.setProgressColor(Color.BLUE);
					}
					if (isDowning) {
						handler.sendEmptyMessageDelayed(new Message().what = 1,
								500);
						mPButton.setProgress(++progress);
					}
				} else {
					mPButton.setProgressText("安装");
					mPButton.setToWaitingProgress(false);
				}
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);

		initProgressButton();

	}

	private void initProgressButton() {
		mPButton = (ProgressButton) findViewById(R.id.progressButton);
		mPButton.setOnProgressButtonListener(this);
		mPButton.setProgressText("下载");

	}

	@Override
	public void onProgressButtonClick() {
		
		if (isDowning) {
			isDowning = false;
			mPButton.setToWaitingProgress(true);
			handler.sendEmptyMessageDelayed(new Message().what = 2, 500);
		} else {
			isDowning = true;
			handler.sendEmptyMessageDelayed(new Message().what = 1, 500);
		}

	}

}
